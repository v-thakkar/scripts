These scripts follows the same format as the old scripts in this [AMDSEV](https://github.com/AMDESE/AMDSEV) repo. Although a lot of options are changed based on distro specific details and outdated options in those scripts.

### Prepare the Host server
Check if sme and sev support is there
```
$ grep -w sev /proc/cpuinfo
...
sme ssbd sev ibpb
```
First, memory encryption needs to be configured in the BIOS settings together with an ASID range for SEV. If asked for a minimal SEV ASID, use a value greater than one to have ASIDs available for SEV-ES guests.
### Prepare Host OS - OpenSUSE Tumbleweed

SEV is not enabled by default, lets enable it through kernel command line:

Append the following in /etc/default/grub

```
GRUB_CMDLINE_LINUX_DEFAULT=".... mem_encryption=on kvm_amd.sev=1"
```
Regenerate grub.cfg and reboot the host
```
 Sudo grub2-mkconfig -o /boot/grub2/grub.cfg
```
For EFI,
```
grub2-mkconfig -o /boot/efi/EFI/opensuse/grub.cfg
```

 To check that sev is being enabled in the kernel:
```
cat /sys/module/kvm_amd/parameters/sev
1
```

### Use QEMU to launch VM(s)
xdg-su -c  "./launch-qemu.sh -hda suse-tumbleweed.qcow2 -cdrom ~/Downloads/openSUSE-Tumbleweed-DVD-x86_64-Snapshot20230910-Media.iso -nosev"

### Use libvirt to launch VM(s)
In openSUSE, one can use YaST to install KVM Server and KVM tools. And then use below command to start encrypted guest.
```
sudo virt-install \
    --arch x86_64 \
    --name "MY-VM" \
    --vcpus 4 \
    --cpu EPYC \ 
    --memory 4096 \
    --machine q35 \
    --memtune hard_limit=4563402 \
    --disk size=32,target.bus=scsi \
    --controller type=scsi,model=virtio-scsi,driver.iommu=on \
    --network network=default,model=virtio,driver.iommu=on \
    --graphics=none --extra-args console=ttyS0 -v \
    --launchSecurity sev,policy=0x3 \
    --boot loader=/usr/share/qemu/ovmf-x86_64-suse-4m.bin,loader.readonly=yes,loader.type=pflash,nvram.template=/usr/share/qemu/ovmf-x86_64-suse-4m-vars.bin,loader_secure=no \
   --install os=opensusetumbleweed
```
If default network is inactive, you can enable it with following command:
```
sudo virsh net-start default
```
// TODO: Add example libvirt xml file

### How to check SEV is enabled in the guest
Check the kernel log buffer
```
#dmesg | grep -i sev
AMD Secure Encrypted Virtualization (SEV) active
```
MSR 0xc0010131 (MSR_AMD64_SEV) can be used to determine if SEV is active. You will have to install `msr-tools` for that.
```
# rdmsr -a 0xc0010131
```

### References
[AMD SEV libvirt documentation](https://libvirt.org/kbase/launch_security_sev.html)
[Jorg Rodel's Blog](https://joros.blog/posts/sev-es-on-tumbleweed/)
